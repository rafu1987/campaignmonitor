<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RZ.Campaignmonitor',
            'CampaignMonitor',
            [
                'CampaignMonitor' => 'display',
            ],
            // non-cacheable actions
            [
                'CampaignMonitor' => 'display',
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    campaignmonitor {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/user_plugin_campaignmonitor.svg
                        title = LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_be.xlf:campaignmonitor_campaignmonitor_pluginWizardTitle
                        description = LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_be.xlf:campaignmonitor_campaignmonitor_pluginWizardDescription
                        tt_content_defValues {
                            CType = list
                            list_type = campaignmonitor_campaignmonitor
                        }
                    }
                }
                show = *
            }
       }'
        );
    },
    $_EXTKEY
);
