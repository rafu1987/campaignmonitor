<?php
namespace RZ\Campaignmonitor\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * CampaignMonitor
 */
class CampaignMonitorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * fieldRepository
     *
     * @var \RZ\Campaignmonitor\Domain\Repository\FieldRepository
     * @inject
     */
    protected $fieldRepository = null;

    /**
     * action display
     *
     * @return void
     */
    public function displayAction()
    {
        // Fields
        $fieldsSettings = explode(",", $this->settings['fields']);
        $fields = $this->fieldRepository->findCustom($fieldsSettings);

        // Action
        $action = $this->settings['action'];

        // Send label
        $sendLabel = $this->settings['sendLabel'];

        $this->view->assign('fields', $fields);
        $this->view->assign('action', $action);
        $this->view->assign('sendLabel', $sendLabel);
    }

}
