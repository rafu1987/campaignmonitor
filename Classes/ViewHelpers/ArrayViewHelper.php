<?php
namespace RZ\Campaignmonitor\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 * @package Campaignmonitor
 * @subpackage ViewHelpers
 */
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class ArrayViewHelper extends AbstractConditionViewHelper
{

    /**
     * @param string $options The options
     * @return string
     */
    public function render($options)
    {
        // Remove white spaces
        $options = str_replace(" ", "", $options);

        // Create array
        $options = explode("\n", $options);

        $optionArr = array();
        foreach ($options as $option) {
            $option = trim($option);

            // Create array
            $option = explode("|", $option);

            $optionArr[] = $option;
        }

        // Rewrite array for better use in Fluid
        $optionsFinal = array();
        foreach ($optionArr as $options) {
            // Title
            $optionSingle['title'] = $options[0];

            // Value
            if ($options[1]) {
                $optionSingle['value'] = $options[1];
            } else {
                unset($optionSingle['value']);
            }

            $optionsFinal[] = $optionSingle;
        }

        return $optionsFinal;
    }
}
