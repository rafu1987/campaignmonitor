﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _start:

=============================================================
Campaign Monitor
=============================================================

.. only:: html

	:Classification:
		campaignmonitor

	:Version:
		0.0.5

	:Language:
		en

	:Description:
		Campaign Monitor subscribe extension

	:Keywords:
		Campaign Monitor

	:Copyright:
		2015

	:Author:
		Raphael Zschorsch

	:Email:
		rafu1987@gmail.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	UsersManual/Index
